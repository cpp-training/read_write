#include <iostream>
#include <array>
#include <vector>


template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::array<T, 3>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::array<T, 4>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}


void outputEulerAngles(const std::array<float, 4>& q);
float computeQuaternionAngle(const std::array<float, 4>& q);

