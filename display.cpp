#include <Eigen/Geometry>

#include "display.h"


void outputEulerAngles(const std::array<float, 4>& q)
{
    const double to_degree = 180./M_PI;
    Eigen::Quaterniond quat(q[0], q[1], q[2], q[3]);

    auto euler = quat.toRotationMatrix().eulerAngles(0, 1, 2) ;

    std::cout << "Euler angles in degrees" << std::endl
              << euler << std::endl ;
}

float computeQuaternionAngle(const std::array<float, 4>& q)
{
    const double to_degree = 180./M_PI;
    Eigen::Quaterniond quat(q[3], q[0], q[1], q[2]);

    // quaternion rotation angle around axis
    float delta_angle = 2.*std::acos(quat.w()) * to_degree;
    // float check = 2.*std::acos(q[3]) * to_degree;

    std::cout << "Quaternion angle: " << std::endl
              << delta_angle << std::endl ;

    return delta_angle;
}

