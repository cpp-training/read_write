#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <cctype>
#include <algorithm>

#include <sstream>
#include <fstream>

#include "display.h"


void sample_file_to_vectors(
    std::vector<std::array<float, 3>> & translations, 
    std::vector<std::array<float, 4>> & quaternions );

bool detect_block_pattern(std::string const & line);

void read_block(std::ifstream & infile, 
    std::vector<std::array<float, 3>> & translations, 
    std::vector<std::array<float, 4>> & quaternions);

void writeQuaternionAngles(std::vector<float> const & angles);



int main()
{
    std::vector<std::string> vec = {
        "Hello", "from", "GCC", __VERSION__, "!" 
    };
    std::cout << vec << std::endl;

    std::vector<std::array<float, 3>> translations ;
    std::vector<std::array<float, 4>> quaternions ;
    std::vector<float> q_angles ;

    sample_file_to_vectors(translations, quaternions);

    for(auto const & elem: quaternions)
    {
        // std::cout << "Raw quaternion : " << std::endl;
        // std::cout << elem << std::endl;
        // outputEulerAngles(elem);

        float angle = computeQuaternionAngle(elem);
        q_angles.push_back(angle);
    }

    // std::cout << q_angles << std::endl ;

    writeQuaternionAngles(q_angles);

}


void writeQuaternionAngles(std::vector<float> const & angles)
{
    std::ofstream fileStream("calib_angles.txt", std::ios::out);

    for(auto const & elem: angles)
    {
        std::ostringstream oss;
        oss << elem;

        fileStream << oss.str() << std::endl;
    }

    fileStream.close();
}


void sample_file_to_vectors(
    std::vector<std::array<float, 3>> & translations, 
    std::vector<std::array<float, 4>> & quaternions )
{
    std::ifstream infile("./sample.txt");

    std::string line;
    while (std::getline(infile, line))
    {
        bool block_detected = false ;

        block_detected = detect_block_pattern(line);

        if(block_detected)
            read_block(infile, translations, quaternions);
    }

}

bool detect_block_pattern(std::string const & line)
{
    const std::string pattern{"-----"};

    std::size_t found = line.find(pattern);
    bool status = false;
    
    if (found!=std::string::npos)
    {
        std::cout << "Block found at: " << found << '\n';
        status = true;
    }

    return status;
}

void read_block(std::ifstream & infile, 
    std::vector<std::array<float, 3>> & translations, 
    std::vector<std::array<float, 4>> & quaternions)
{
        float tx, ty, tz;
        float qx, qy, qz, qw;

        std::string line1, line2, line3, line4;
        std::stringstream iss1, iss2, iss3, iss4, iss5, iss6, iss7;

        // read 3 translation floats
        std::getline(infile, line1);
        std::getline(infile, line2);
        std::getline(infile, line3);

        iss1 << line1 ;
        iss2 << line2 ;
        iss3 << line3 ;

        int status = 1;
        // copy 3 translation floats
        iss1 >> tx ;
        iss2 >> ty ;
        iss3 >> tz ;

        std::array<float, 3> read_tr{tx, ty, tz} ;
        translations.push_back(read_tr);

        // read 4 translation floats
        std::getline(infile, line1);
        std::getline(infile, line2);
        std::getline(infile, line3);
        std::getline(infile, line4);

        // line1.erase(std::remove_if(line1.begin(), line1.end(), ::isspace), line1.end());

        // std::cerr << std::boolalpha << iss1.good() << " " << iss1.eof() << " " <<
        //           << iss1.fail() << " " << iss1.bad() << "\n";

        iss1.clear(); iss1.str(line1);
        iss2.clear(); iss2.str(line2);
        iss3.clear(); iss3.str(line3);
        iss4 << line4 ;

        // copy 4 quaternion params
        iss1 >> qx ;
        iss2 >> qy ;
        iss3 >> qz ;
        iss4 >> qw ;

        std::array<float, 4> read_quat{qx, qy, qz, qw} ;
        quaternions.push_back(read_quat);
}

// if(!status)
// {
//     std::cout << "Can't read translation params" << std::endl;
// }


